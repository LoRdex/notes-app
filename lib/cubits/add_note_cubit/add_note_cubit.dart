import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:hive/hive.dart';
import 'package:meta/meta.dart';
import 'package:notes_app/models/note_model.dart';

part 'add_note_state.dart';

class AddNoteCubit extends Cubit<AddNoteState> {
  AddNoteCubit() : super(AddNoteInitial());
Color color=Color(0xffDFBBB1);
  addNote(NoteModel noteModel) async {
    noteModel.color=color.value;
    emit(AddNoteLoading());
    try {
      await Future.delayed(Duration(seconds: 1)); // Artificial delay for testing
      var notesBox = Hive.box<NoteModel>("noteBox");
      await notesBox.add(noteModel);
      emit(AddNoteSuccess());
    } catch (value) {
      emit(AddNoteFailure(value.toString())); // Ensure you emit the failure state
    }
  }
}

