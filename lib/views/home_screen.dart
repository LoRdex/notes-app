import 'package:flutter/material.dart';
import 'package:notes_app/views/widgets/add_note_bottom.dart';
import 'package:notes_app/views/widgets/note_screen_body.dart';

class NoteScreen extends StatelessWidget {
  const NoteScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showModalBottomSheet(isScrollControlled: true,context: context, builder: (context) => AddNoteBottomSheet());
        },
        child: Icon(Icons.add),
      ),
      body: const NoteScreenBody(),
    );
  }
}

