import 'package:flutter/material.dart';

import 'custom_search_tap.dart';

class CustomAppBar extends StatelessWidget {

IconData icon;
String title;
final void Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return  Row(
      children: [
        Text(
          '$title',
          style: TextStyle(fontSize: 28, fontFamily: 'Poppins'),
        ),
        Spacer(),
        CustomSearchTap(icon,onPressed),
      ],
    );
  }

CustomAppBar(this.icon,this.title, this.onPressed);
}
