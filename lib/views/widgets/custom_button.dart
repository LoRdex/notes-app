import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  void Function()? onTap;
bool isLoading=false;
  CustomButton(this.isLoading,this.onTap,);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
    onTap:onTap,
      child: Container(
        decoration: BoxDecoration(
            color: Colors.pinkAccent, borderRadius: BorderRadius.circular(16)),
        width: MediaQuery.of(context).size.width,
        height: 60,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24,vertical: 8),
          child: Center(
            child: isLoading ? LinearProgressIndicator(color: Colors.amberAccent,minHeight: 1,)  : Text(
              'Save Note',
              style: TextStyle(fontSize: 20, color: Colors.amber),
            ),
          ),
        ),
      ),
    );
  }
}
