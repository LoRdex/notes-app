import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:notes_app/cubits/notes_cubit/notes_cubit.dart';
import 'package:notes_app/models/note_model.dart';
import 'package:notes_app/views/widgets/edit_note_screen.dart';
import 'package:notes_app/views/widgets/note_edit_body.dart';

class NoteItem extends StatelessWidget {
  NoteItem(this.note);
  final NoteModel note;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) => NoteEditScreen(note: note,),));
      },
      child: Container(padding: EdgeInsets.only(bottom: 24,top: 24,left: 12),
        decoration: BoxDecoration(
            color: Color(note.color), borderRadius: BorderRadius.circular(16)),
        child: Column(crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            ListTile(
              title: Text(
                "${note.title}",
                style: TextStyle(color: Colors.black, fontFamily: 'Poppins',fontSize: 26),
              ),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Text("${note.subTitle}",
                    style: TextStyle(color: Colors.black.withOpacity(.5), fontFamily: 'Poppins',fontSize: 20)),
              ),
              trailing: IconButton(
                  onPressed: () {note.delete();
                    BlocProvider.of<NotesCubit>(context).fetchAllNotes();},
                  icon: Icon(
                    Icons.delete,
                    color: Colors.black,
                    size: 30,
                  )),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 25),
              child: Text("${note.date}",style: TextStyle(color: Colors.black.withOpacity(.4),)),
            )
          ],
        ),
      ),
    );
  }

}
