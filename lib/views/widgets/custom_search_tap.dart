import 'package:flutter/material.dart';

class CustomSearchTap extends StatelessWidget {
  CustomSearchTap(this.icon, this.onPressed);

IconData icon;
  @override  final void Function()? onPressed;

  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        width: 45,
        height: 45,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            color: Colors.white.withOpacity(.05)),
        child: IconButton(onPressed: onPressed, icon: Icon(icon)),
      ),
    );
  }

}
