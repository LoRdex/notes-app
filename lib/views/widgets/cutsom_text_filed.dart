import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomTextFiled extends StatelessWidget {

String? hintText;
int? maxLines;
int? minLines;
void Function(String?)? onSaved;
void Function(String)? onChanged;
CustomTextFiled(this.hintText, this.maxLines,this.minLines,this.onSaved,this.onChanged);

  @override
  Widget build(BuildContext context) {
    return TextFormField(maxLines: maxLines,minLines:minLines,
      onChanged:onChanged ,
      onSaved:onSaved ,
      validator:(value) {
        if(value?.isEmpty ?? true)
          {
            return 'Field is Required';
          }else
            return null;
      },
      decoration: InputDecoration(hintText: hintText,

        hintStyle:TextStyle(color: Colors.amber) ,
        border: buildOutlineInputBorder(),
        enabledBorder: buildOutlineInputBorder(),

      ),
    );
  }

  OutlineInputBorder buildOutlineInputBorder() =>
      OutlineInputBorder(borderRadius: BorderRadius.circular(12),borderSide: BorderSide(color: Colors.white));
}
