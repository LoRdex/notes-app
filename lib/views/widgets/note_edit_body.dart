import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:notes_app/cubits/notes_cubit/notes_cubit.dart';
import 'package:notes_app/models/note_model.dart';
import 'package:notes_app/views/widgets/custom_app_bar.dart';

import 'cutsom_text_filed.dart';

class NoteEditBody extends StatefulWidget {
  const NoteEditBody({super.key, required this.note});

  final NoteModel note;

  @override
  State<NoteEditBody> createState() => _NoteEditBodyState();
}

class _NoteEditBodyState extends State<NoteEditBody> {
  String? title, subTitle;


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 50, left: 16, right: 16),
      child: Column(
        children: [
          CustomAppBar(Icons.done, "Edit Note", () {widget.note.title=title??widget.note.title;
          widget.note.subTitle=subTitle??widget.note.subTitle;
          widget.note.save();
          BlocProvider.of<NotesCubit>(context).fetchAllNotes();
          Navigator.pop(context);
          }),
          SizedBox(
            height: 100,
          ),
          CustomTextFiled(widget.note.title, 1, 1, (value) {}, (value) {
            title = value;
          }),
          SizedBox(
            height: 60,
          ),
          CustomTextFiled(widget.note.subTitle, 5, 1, (value) {}, (value) {
            subTitle = value;
          }),
          SizedBox(
            height: 50,
          ),
        ],
      ),
    );
  }
}
