import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:notes_app/cubits/notes_cubit/notes_cubit.dart';
import 'package:notes_app/views/widgets/custom_app_bar.dart';

import 'custom_noteitem.dart';
import 'notes_list_view.dart';

class NoteScreenBody extends StatefulWidget {
  const NoteScreenBody({super.key});

  @override
  State<NoteScreenBody> createState() => _NoteScreenBodyState();
}

class _NoteScreenBodyState extends State<NoteScreenBody> {
  @override
  void initState(){
    BlocProvider.of<NotesCubit>(context).fetchAllNotes();
    super.initState();
  }
  Widget build(BuildContext context) {
    return  Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          const SizedBox(
            height: 45,
          ),
           CustomAppBar(Icons.search,"Notes",(){}),
        const   SizedBox(
            height: 30,
          ),
          Expanded(child: NotesListView()),


        ],
      ),
    );
  }
}
